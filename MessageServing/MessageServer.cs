﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;


namespace MessageServing
{
    public class MessageServer
    {
		private volatile Boolean continueListening = true;

		public event MessageEventHandler NewMessageReceived;

		private int defaultPort = 5508;

		private Thread socketListenerThread;

		private volatile UdpClient client;

		public string ClientName = "Henk";

		private object clientListLock = new object();

		private volatile List<ClientInfo> discoveredClients = null;

		public void startServer()
		{
			initSocket();
			startListener();
		}


		public void sendTextMessage(IPAddress receiver, string msg)
		{
			MessageContainer cont = new MessageContainer();
			cont.MessageType = MessageTypes.Text;
			cont.SenderName = ClientName;
			cont.Text = msg;
			sendMessage(cont, receiver);
		}


		public void stopServer()
		{
			if (client != null)
			{
				continueListening = false;
				client.Close();
				Thread.Sleep(100);	// Allow time for socket to close

				client = null;
				if (socketListenerThread.ThreadState != ThreadState.Stopped)
				{
					socketListenerThread.Abort();
				}
			}
		}

		private void startListener()
		{
			socketListenerThread = new Thread(listenerThreadMethod);
			socketListenerThread.Name = "SocketListener";
			socketListenerThread.IsBackground = true;
			socketListenerThread.Start();
		}



		private void initSocket()
		{
			client = new UdpClient(defaultPort);
			client.DontFragment = true;
		}



		public List<ClientInfo> getAvailableClients()
		{
			List<ClientInfo> retVal = null;
			discoveredClients = new List<ClientInfo>();

			sendDiscoveryRequest();
			Thread.Sleep(400);

			lock (clientListLock)
			{
				retVal = new List<ClientInfo>(discoveredClients);
				discoveredClients = null;
			}
			retVal.Sort();
			return retVal;
		}



		private void sendDiscoveryRequest()
		{
			MessageContainer cont = new MessageContainer();
			cont.MessageType = MessageTypes.DiscoverRequest;
			cont.SenderName = ClientName;
			sendMessage(cont, IPAddress.Parse("255.255.255.255"));
		}



		private void sendMessage(MessageContainer msg, IPAddress addr)
		{
			byte[] data = encodeMessage(msg);

			IPEndPoint ep = new IPEndPoint(addr, defaultPort);
			if(client != null) client.Send(data, data.Length, ep);
		}


		private void listenerThreadMethod(Object na)
		{
			IPEndPoint ep;
			byte[] data;
			MessageContainer msgContainer;
			ClientInfo sender;
			
			try
			{
				while (continueListening)
				{
					Thread.Sleep(0);
					ep = new IPEndPoint(0, 0);

					if (client.Available == 0)
					{
						Thread.Sleep(20);
						continue;
					}
					data = client.Receive(ref ep);

					if (data.Length == 0)
					{
						Thread.Sleep(20);
						continue;
					}

					msgContainer = decodeReceivedMessage(data);

					sender = new ClientInfo();
					sender.IPAddress = new IPEndPoint(ep.Address, ep.Port);
					sender.Name = msgContainer.SenderName;

					switch (msgContainer.MessageType)
					{
						case MessageTypes.Text:
							dispatchMessage(msgContainer, sender);
							break;
						case MessageTypes.DiscoverRequest:
							responseToDiscoveryRequest(msgContainer);
							break;
						case MessageTypes.DiscoverAck:
							addDiscoverAckToList(msgContainer, sender);
							break;
					}

				}
			}
			catch (ThreadAbortException) { }
			catch (ThreadInterruptedException) { }
			catch (Exception exc)
			{
				Console.WriteLine("Internal socketListener exception: " + exc.ToString());
			}

			stopServer();	// Only needed when the server quits due to an error

		}

		private void addDiscoverAckToList(MessageContainer msgContainer, ClientInfo sender)
		{
			lock (clientListLock)
			{
				if (discoveredClients != null)
				{
					if (!discoveredClients.Contains(sender))
					{
						discoveredClients.Add(sender);
					}
				}
			}
		}


		private void responseToDiscoveryRequest(MessageContainer request)
		{
			MessageContainer cont = new MessageContainer();
			cont.MessageType = MessageTypes.DiscoverAck;
			cont.SenderName = ClientName;
			sendMessage(cont, IPAddress.Parse("255.255.255.255"));
		}


		private void dispatchMessage(MessageContainer msg, ClientInfo sender)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object na)
			{
				if (NewMessageReceived != null)
				{
					NewMessageReceived.Invoke(this, new MessageEventArgs(msg, sender));
				}
			}));
		}


		private byte[] encodeMessage(MessageContainer msg)
		{
			byte[] data;
			string xml = msg.SaveToXmlString();
			data = Encoding.ASCII.GetBytes(xml);
			return data;
		}

		private MessageContainer decodeReceivedMessage(byte[] datagramData)
		{
			MessageContainer retVal = null;
			string xml = GetString(datagramData);
			XmlElement xmlElem = GetXmlElement(xml);
			retVal = MessageContainer.Load(xmlElem);
			return retVal;
		}

		static byte[] GetBytes(string str)
		{
			return Encoding.ASCII.GetBytes(str);
		}

		static string GetString(byte[] bytes)
		{
			return Encoding.ASCII.GetString(bytes);
		}

		private static XmlElement GetXmlElement(string xml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);
			return doc.DocumentElement;
		}

    }
}
