﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MessageServing
{
	public class ClientInfo : IEqualityComparer<ClientInfo>, IComparable<ClientInfo>
	{

		public string Name { get; set; }

		public IPEndPoint IPAddress { get; set; }


		public bool Equals(ClientInfo x, ClientInfo y)
		{
			if ((x.IPAddress == y.IPAddress) && (x.Name == y.Name)) return true;
			return false;
		}

		public int GetHashCode(ClientInfo obj)
		{
			if ((Name == null) && (IPAddress == null)) return 123;
			if ((Name == null) && (IPAddress != null)) return IPAddress.GetHashCode();
			if ((Name != null) && (IPAddress == null)) return Name.GetHashCode();
			unchecked
			{
				return IPAddress.GetHashCode() + Name.GetHashCode();
			}
		}

		public int CompareTo(ClientInfo other)
		{
			if ((Name == null) && (IPAddress == null))
			{
				if ((other.Name != null) || (other.IPAddress != null)) return -1;
				else return 0;
			}

			if ((Name == null) && (IPAddress != null))
			{
				if ((other.Name != null) && (other.IPAddress != null)) return -1;
				if ((other.Name == null) && (other.IPAddress != null)) return 1;
				return 1;
			}

			if ((Name != null) && (IPAddress == null))
			{
				if ((other.Name != null) && (other.IPAddress != null)) return 1;
				if ((other.Name == null) && (other.IPAddress != null)) return -1;
				return 1;
			}

			int strVal = this.Name.CompareTo(other.Name);
			if (strVal == 0)
			{
				return this.IPAddress.ToString().CompareTo(other.IPAddress.ToString());
			}
			else
			{
				return strVal;
			}
		}
	}
}
