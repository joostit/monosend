﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageServing
{
	public enum MessageTypes
	{
		Text,
		DiscoverRequest,
		DiscoverAck
	}
}
