﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageServing
{

	public delegate void MessageEventHandler(object sender, MessageEventArgs args);

	public class MessageEventArgs : EventArgs
	{

		public MessageContainer Message { get; private set; }

		public ClientInfo SenderInfo { get; private set; }

		public MessageEventArgs(MessageContainer msg, ClientInfo senderInfo)
		{
			Message = msg;
			SenderInfo = senderInfo;
		}

	}
}
