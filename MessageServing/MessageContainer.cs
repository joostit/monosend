﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MessageServing
{
	[XmlRoot]
	public class MessageContainer : XmlDataClassBase<MessageContainer>
	{
		public string Text { get; set; }

		public MessageTypes MessageType { get; set; }

		public String SenderName { get; set; }

	}
}
