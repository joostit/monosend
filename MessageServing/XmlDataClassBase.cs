﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MessageServing
{
	/// <summary>
	/// Defines the base class for saveable and loadable settings
	/// </summary>
	/// <typeparam name="SettingsType"></typeparam>
	public abstract class XmlDataClassBase<InheritorType>
	{
		/// <summary>
		/// A description that is added to the top of the settings file when it is saved
		/// </summary>
		[XmlIgnore]
		protected string XmlDescription = "(c) Dialog Semiconductor";

		/// <summary>
		/// Returns an object, loaded from the specified XML file
		/// </summary>
		/// <param name="path">The path to the XML file</param>
		/// <returns>The object that has been loaded from XML</returns>
		public static InheritorType Load(String path)
		{
			InheritorType retVal = default(InheritorType);
			try
			{
				XmlSerializer deserializer = new XmlSerializer(typeof(InheritorType));
				TextReader textReader = new StreamReader(path);
				retVal = (InheritorType)deserializer.Deserialize(textReader);
				textReader.Close();
				textReader.Dispose();
			}
			catch (Exception exc)
			{
				throw new Exception("Unable to load settings from XML: " + exc.Message, exc);
			}
			return retVal;
		}


		/// <summary>
		/// Returns an object, loaded from the specified XML file
		/// </summary>
		/// <param name="xml">The XMl elemement to load from</param>
		/// <returns>The object that has been loaded from XML</returns>
		public static InheritorType Load(XmlElement xml)
		{
			InheritorType retVal = default(InheritorType);
			try
			{
				XmlSerializer deserializer = new XmlSerializer(typeof(InheritorType));
				using (TextReader reader = new StringReader(xml.OuterXml))
				{
					retVal = (InheritorType)deserializer.Deserialize(reader);
				}
			}
			catch (Exception exc)
			{
				throw new Exception("Unable to load settings from XML: " + exc.Message, exc);
			}
			return retVal;
		}


		/// <summary>
		/// Serializes the object into an XML string
		/// </summary>
		/// <returns></returns>
		public string SaveToXmlString()
		{
			string retVal = null;
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(InheritorType));
			XmlSerializerNamespaces emptyNamespace = new XmlSerializerNamespaces();
			emptyNamespace.Add("", "");
			MemoryStream memStream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(memStream);
			xmlSerializer.Serialize(streamWriter, this, emptyNamespace);
			memStream.Position = 0;
			StreamReader streamReader = new StreamReader(memStream);
			XmlDocument serializedXML = new XmlDocument();
			serializedXML.Load(streamReader);
			retVal = serializedXML.OuterXml;
			return retVal;
		}


		/// <summary>
		/// Saves the object to an XML element
		/// </summary>
		/// <returns></returns>
		public XmlElement SaveToXmlElement()
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(SaveToXmlString());
			return doc.DocumentElement;
		}


		/// <summary>
		/// Saves this object as an XML file
		/// </summary>
		/// <param name="path">The path to where the file should be saved</param>
		public void SaveToFile(string path)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(InheritorType));
			FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write);
			XmlWriterSettings sets = new XmlWriterSettings();
			sets.CheckCharacters = true;
			sets.ConformanceLevel = ConformanceLevel.Document;
			sets.Indent = true;
			sets.NewLineHandling = NewLineHandling.Replace;

			XmlWriter writer = XmlWriter.Create(stream, sets);
			writer.WriteStartDocument();
			writer.WriteComment(XmlDescription);
			serializer.Serialize(writer, this);
			writer.WriteEndDocument();
			writer.Flush();
			stream.Close();
			stream.Dispose();
		}
	}
}