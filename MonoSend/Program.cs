﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonoSend
{
	class Program
	{
		static void Main(string[] args)
		{
			printFrameworkinfo();

			MonoSendConsoleInterface iFace = new MonoSendConsoleInterface();
			iFace.Start(Environment.UserName + "@" + Environment.MachineName);
		}


		private static void printFrameworkinfo()
		{
			Console.WriteLine("Hello CIM on .NET3.0");
			Console.WriteLine("");
			Type t = Type.GetType("Mono.Runtime");
			if (t != null)
				Console.WriteLine("Running on the Mono VM");
			else
				Console.WriteLine("Running on Microsoft .NET");

			Console.WriteLine("Version: " + Environment.Version.ToString());
			Console.WriteLine("");

			Console.WriteLine("OS: " + Environment.OSVersion.ToString());
			Console.WriteLine("User: " + Environment.UserName);
			Console.WriteLine("Machine: " + Environment.MachineName);
		}

	}
}
