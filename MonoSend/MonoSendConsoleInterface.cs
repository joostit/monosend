﻿using MessageServing;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoSend
{
	public class MonoSendConsoleInterface
	{

		public MonoSendConsoleInterface()
		{

		}

		MessageServer server = new MessageServer();

		public void Start(string name)
		{
			server.NewMessageReceived += server_NewMessageReceived;
			server.ClientName = name;
			server.startServer();
			runInterface();
			server.stopServer();
		}



		private void runInterface()
		{
			Boolean go = true;
			char command;
			while (go)
			{
				Console.WriteLine("");
				Console.WriteLine("MonoSend chat application");
				Console.WriteLine(" F --> Find clients");
				Console.WriteLine(" S --> Send message ");
				Console.WriteLine(" Q --> Quit ");
				Console.WriteLine("...");
				Console.WriteLine("");
				command = Console.ReadKey(true).KeyChar;

				switch (command)
				{
					case 'f':
					case 'F':
						Console.WriteLine("");
						Console.WriteLine("Searching for available clients on network...");
						findAndPrintClients();
						break;

					case 's':
					case 'S':
						sendMessage();
						break;

					case 'q':
					case 'Q':
						go = false;
						break;

					default:
						Console.WriteLine("Invalid input");
						break;
				}
			}
		}

		private void sendMessage()
		{
			Console.WriteLine("");
			List<ClientInfo> clients = findAndPrintClients();
			ClientInfo recipient = null;
			Console.WriteLine("Enter recipient number... (press C to Cancel)");
			char command = Console.ReadKey(true).KeyChar;
			if ((command == 'c') || (command == 'C')) return;

			try
			{
				int nr = Convert.ToInt32("" + command);
				recipient = clients[nr];
			}
			catch
			{
				Console.WriteLine("Invalid input");
				Console.WriteLine("");
				return;
			}

			Console.WriteLine("Enter message for " + recipient.Name + ": (press Enter when ready)");
			string message = Console.ReadLine();
			server.sendTextMessage(recipient.IPAddress.Address, message);
		}

		private List<ClientInfo> findAndPrintClients()
		{

			Console.WriteLine("");
			List<ClientInfo> clients = server.getAvailableClients();
			Console.WriteLine("");
			for (int i = 0; i < clients.Count; i++)
			{
				ClientInfo nfo = clients[i];
				Console.WriteLine(i.ToString() + " >>  " + nfo.Name + "   at   " + nfo.IPAddress.ToString());
			}

			Console.WriteLine("---");
			return clients;
		}


		void server_NewMessageReceived(object sender, MessageEventArgs args)
		{
			printNewMessage(args.Message, args.SenderInfo);
		}


		private void printNewMessage(MessageContainer msg, ClientInfo senderInfo)
		{
			Console.WriteLine("");
			Console.WriteLine("===");
			Console.WriteLine("Received message from " + msg.SenderName + " (" + senderInfo.IPAddress.ToString() + ")");
			Console.WriteLine( msg.Text);
			Console.WriteLine("===");
			Console.WriteLine("");
		}


	}
}
